/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  Image,
  Modal
} from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
export default class App extends Component<Props> {
  state = {
    modalVisible: false
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.column}>
          <View style={styles.Box}>
            <Text
              onPress={() => {
                this.setModalVisible(true);
              }}
            >
              Hi
            </Text>
          </View>
          <View style={styles.Box}>
            <Text>Hi</Text>
          </View>
          <View style={styles.Box}>
            <Text>Hi</Text>
          </View>
        </View>
        <View style={styles.column}>
          <View style={styles.Box}>
            <Text>Hi</Text>
          </View>
          <View style={styles.Box}>
            <Text>Hi</Text>
          </View>
          <View style={styles.Box}>
            <Text>Hi</Text>
          </View>
        </View>
        <View style={{ padding: 10, margin: 30 }}>
          <Modal
            style={{ height: 50, width: 50 }}
            animationType="fade"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
            }}
          >
            <View>
              <View>
                <Text>Hello World!</Text>

                <TouchableHighlight
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Text>Cancle</Text>
                </TouchableHighlight>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  row: {
    flex: 1,
    flexDirection: "column"
  },
  column: {
    flex: 1,
    flexDirection: "row"
  },
  Box: {
    flex: 1,
    backgroundColor: "green",
    padding: 10,
    margin: 8
  }
});
